public class Message {

    private final int channelId;
    private final String message;

    public Message(int channelId, String message) {
        this.channelId = channelId;
        this.message = message;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return new StringBuilder(String.valueOf(this.channelId)).append("-").append(this.message).toString();
    }

}
