import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class TCPServer {

    private static int port = 8080;
    private static int responseNumber = 0;

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(port);
        while (true) {
            final Socket socket = server.accept();
            new Thread(() -> {
                try {
                    byte[] buff = new byte[1024 * 1024 * 10];
                    InputStream is = socket.getInputStream();
                    final OutputStream os = socket.getOutputStream();
                    while (true) {
                        int length = is.read(buff);
                        if (length < 0) {
                            System.out.println("Cierro socket");
                            socket.close();
                            break;
                        }
                        String rawMessage = new String(buff, 0, length, Charset.defaultCharset());
                        Message message = MessageUtils.parseMessage(rawMessage);
                        System.out.println("Channel " + message.getChannelId() + " ask for " + message.getMessage());
                        new Thread(() -> {
                            try {
                                Thread.sleep(1000);
                                if (socket.isClosed()) {
                                    return;
                                }
                                Message response = new Message(message.getChannelId(), "Response=" + responseNumber++);
                                System.out.println("Sending response " + response.getMessage() + " to channel: " + response.getChannelId());
                                os.write(response.toString().getBytes(Charset.defaultCharset()));
                            } catch (Exception e) {
                                try {
                                    socket.close();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                e.printStackTrace();
                                return;
                            }
                        }).start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

}
